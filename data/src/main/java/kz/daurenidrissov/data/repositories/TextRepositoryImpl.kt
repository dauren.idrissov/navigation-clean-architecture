package kz.daurenidrissov.data.repositories

import android.content.SharedPreferences
import kz.daurenidrissov.domain.repositories.TextRepository

private const val TEXT_PREF_KEY = "TEXT_PREF_KEY"

class TextRepositoryImpl(private val sharedPreferences: SharedPreferences) : TextRepository {

    override fun put(text: String) {
        val editor = sharedPreferences.edit()
        editor.putString(TEXT_PREF_KEY, text)
        editor.apply()
    }
}