package kz.daurenidrissov.data.di_modules

import android.preference.PreferenceManager
import kz.daurenidrissov.data.repositories.TextRepositoryImpl
import kz.daurenidrissov.domain.repositories.TextRepository
import org.koin.dsl.module

fun getRepositoryModule() = module {
    single<TextRepository> {
        TextRepositoryImpl(
            PreferenceManager.getDefaultSharedPreferences(get())
        )
    }
}