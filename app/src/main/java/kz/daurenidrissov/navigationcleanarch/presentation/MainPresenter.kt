package kz.daurenidrissov.navigationcleanarch.presentation

import kz.daurenidrissov.domain.use_cases.TextChangeUseCase
import kz.daurenidrissov.navigationcleanarch.broadcast.ConnectionReceiver
import kz.daurenidrissov.navigationcleanarch.ui.MainFragment
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.lang.IllegalArgumentException

class MainPresenter(private val view: MainFragment) : KoinComponent, ConnectionReceiver.ConnectionReceiverListener{

    val useCase by inject<TextChangeUseCase>()

    fun onClickSendButton(text: String) =
        try {
            useCase.execute(text)
        } catch (e: IllegalArgumentException) {
            view.onError()
            null
        }

    override fun onNetworkConnectionChanged(isConnected: Boolean, isChanged: Boolean) {
        if(isConnected && isChanged) {
            view.onConnected()
        } else if(!isConnected){
            view.onConnectionLost()
        }
    }
}