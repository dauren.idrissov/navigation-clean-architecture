package kz.daurenidrissov.navigationcleanarch

import android.app.Application
import kz.daurenidrissov.data.di_modules.getRepositoryModule
import kz.daurenidrissov.domain.di_modules.getUseCaseModule
import kz.daurenidrissov.navigationcleanarch.broadcast.ConnectionReceiver
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp: Application() {

    companion object{
        @get:Synchronized
        lateinit var instance: MyApp
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        startKoin {
            androidContext(this@MyApp)
            modules(
                listOf(
                    getUseCaseModule(),
                    getRepositoryModule()
                )
            )
        }
    }

    fun setConnectionListener(listener: ConnectionReceiver.ConnectionReceiverListener) {
        ConnectionReceiver.connectionReceiverListener = listener
    }
}