package kz.daurenidrissov.navigationcleanarch.ui

import android.app.AlertDialog
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_main.*
import kz.daurenidrissov.navigationcleanarch.broadcast.ConnectionReceiver
import kz.daurenidrissov.navigationcleanarch.MyApp
import kz.daurenidrissov.navigationcleanarch.R
import kz.daurenidrissov.navigationcleanarch.presentation.MainPresenter

class MainFragment : Fragment() {

    val presenter by lazy {
        MainPresenter(view = this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.registerReceiver(ConnectionReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        MyApp.instance.setConnectionListener(presenter)

        sendBT.setOnClickListener {
            val text = presenter.onClickSendButton(textET.text.toString())
            text?.let { s ->
                val action = MainFragmentDirections.actionMainFragmentToNewFragment(s)
                it.findNavController().navigate(action)
            }
        }
    }

    fun onError() {
        Toast.makeText(context, "Minimum 5 characters required!", Toast.LENGTH_SHORT).show()
    }

    fun onConnected() {
        val alertBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
        alertBuilder.setMessage("Connection restored! Do you want to refresh this page?")
            .setCancelable(false)
            .setPositiveButton("Refresh") { dialog, which ->
                requireFragmentManager().beginTransaction().detach(this).attach(this).commit()
            }
            .setNegativeButton("Cancel") { dialog, which ->
                dialog.cancel()
            }.create()
            .setTitle("Internet Connection")

        alertBuilder.show()
    }

    fun onConnectionLost() {
        Toast.makeText(context, "Connection lost!", Toast.LENGTH_SHORT).show()
    }
}