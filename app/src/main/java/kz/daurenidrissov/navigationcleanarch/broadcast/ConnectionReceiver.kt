package kz.daurenidrissov.navigationcleanarch.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import kz.daurenidrissov.navigationcleanarch.MyApp

class ConnectionReceiver : BroadcastReceiver() {

    interface ConnectionReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean, isChanged: Boolean)
    }

    companion object {
        var connectionReceiverListener: ConnectionReceiverListener? = null
        val isConnected: Boolean
        get() {
            val cm = MyApp.instance.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return (activeNetwork != null && activeNetwork.isConnectedOrConnecting)
        }
        var wasConnected: Boolean? = null
    }

    override fun onReceive(context: Context, intent: Intent) {
        //val isConnected = checkConnection(context)
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = (activeNetwork != null && activeNetwork.isConnectedOrConnecting)

        val isFirstUsage: Boolean = wasConnected == null && !isConnected
        var isConnectionChanged: Boolean
        if(wasConnected != null) {
            isConnectionChanged = wasConnected != isConnected
        } else {
            isConnectionChanged = !isConnected
        }
        wasConnected = isConnected

        if(connectionReceiverListener != null && isConnectionChanged) {
            connectionReceiverListener!!.onNetworkConnectionChanged(isConnected, isFirstUsage||isConnectionChanged)
        }
    }
}
