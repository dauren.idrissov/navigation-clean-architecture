package kz.daurenidrissov.domain.use_cases

import kz.daurenidrissov.domain.repositories.TextRepository
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.lang.IllegalArgumentException

class TextChangeUseCase : KoinComponent {

    private val textRepository by inject<TextRepository>()

    fun execute(text: String): String {
        if(text.length >= 5) {
            textRepository.put(text)
            return text.toUpperCase()
        } else throw IllegalArgumentException()
    }
}