package kz.daurenidrissov.domain.di_modules

import kz.daurenidrissov.domain.use_cases.TextChangeUseCase
import org.koin.dsl.module

fun getUseCaseModule() = module {
    single { TextChangeUseCase() }
}