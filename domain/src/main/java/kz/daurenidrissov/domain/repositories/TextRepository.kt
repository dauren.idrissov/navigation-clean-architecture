package kz.daurenidrissov.domain.repositories

interface TextRepository {
    fun put(text: String)
}